#!/usr/bin/env python3

import os
import sys
import convert_to_squad_format as convert

QA_DIR = "../qa/"
QA_FILE = QA_DIR + "wikipedia-train.json"
SQUAD_FILE = QA_DIR + "squad_wikipedia-train.json"
WEB_DIR = "../evidence/web/"
WIKI_DIR = "../evidence/wikipedia"

os.system("python3 convert_to_squad_format.py" \
    + " --triviaqa_file " + QA_FILE \
    + " --squad_file " + SQUAD_FILE \
    + " --wikipedia_dir " + WIKI_DIR \
    + " --web_dir " + WEB_DIR)