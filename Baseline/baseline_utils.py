import nltk
import math

WPA = 3     # Average words per answer, based on training data

def vectorize(sentence, words):
    """Return a vector with the counts of each word in words that occurs in the given sentence."""

    tokens = set(nltk.word_tokenize(sentence))
    vector = dict((word, 0) for word in words)
    
    for token in tokens:
        try:
            vector[token] += 1
        except KeyError:
            # I *think* KeyErrors just occur when words are in the question that aren't in the context
            # This would be an automatic 0 for the shared word count
            # Rather than adding the token with vector[token] = 0, just leave it out so we don't cause problems
            # with vector_compare()
            pass

    return vector


def vector_compare(v1, v2):
    """Return number of shared words in two vectors of equal length.""" 

    shared = 0

    for i in range(0, len(v1)):
        if v1[i] == v2[i]:
            shared += v1[i]
        else:
            shared += min(v1[i], v2[i])
        
    return shared


def counts(context):
    """Given some context, return
    C(w), count of each word in context for each word
    IC(w) = log(1 + 1 / C(w)).

    For sliding window algorithm specified by Richardson, et. al (2013)."""

    words = nltk.word_tokenize(context)
    
    C = {}      # Note that for purposes of counting total instances of words, case is not considered
    for word in words:
        try:
            C[word.lower()] += 1        # Increase count of previously seen word
        except KeyError:
            C[word.lower()] = 1         # Starting count of new word

    IC = {}
    for word in set(words):
        IC[word] = math.log(1 + 1 / C[word.lower()])

    return C, IC


def generate_answers(sentence):
    """Generate sets of words that are potential answers."""

    sentence = nltk.word_tokenize(sentence)
    return [sentence[i:i+WPA] for i in range(0, len(sentence) - 1)]


def sliding_window(C, IC, context, question, sentence):
    P = nltk.word_tokenize(context)     # Passage
    Q = nltk.word_tokenize(question)    # Question words
    A = generate_answers(sentence)      # Answer words

    SW = [None] * len(A)
    for i in range(0, len(A)):
        S = set(list(A[i]) + list(Q))       # Question words + Answer words

        SW[i] = 0       
        for j in range(0, len(P)):
            s = 0
            for w in range(0, len(S)):
                if (j + w < len(P)) and (P[j + w] in S):
                    s += IC[P[j + w]]
                else:
                    s += 0

            if SW[i] < s:
                SW[i] = s

    return ' '.join(A[SW.index(max(SW))])


def sentence_prediction(question_vector, sentence_vectors):
    """Predict which sentence will contain the answer to the question.
    Model: The sentence with the answer to the question is the one with that shares the most words
    with the question.
    Return value is index of predicted sentence, in list of sentences which corresponds to sentence_vectors."""

    prediction = 0      # Default guess is first sentence
    max_shared = 0

    for i, sentence_vector in enumerate(sentence_vectors):
        shared = vector_compare(list(question_vector.values()), list(sentence_vector.values()))
        
        if max_shared < shared:
            max_shared = shared
            prediction = i

    return prediction
