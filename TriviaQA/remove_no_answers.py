#!/usr/bin/env python3

import json

sfile = "squad_wikipedia-train.json"
nfile = "squad_wikipedia-train_answers-only.json"

with open(sfile, "r") as DF:
   data = DF.read()

data = json.loads(data)
ndata = { "data": [] }

for paragraph in data["data"]:
    nparagraph = { "paragraphs": [] }

    for context in paragraph["paragraphs"]:
        if len(context["qas"][0]["answers"]) != 0:
            nparagraph["paragraphs"].append(context)

    if len(nparagraph["paragraphs"]) != 0:
        ndata["data"].append(nparagraph)

with open(nfile, "w+") as NF:
    json.dump(ndata, NF)