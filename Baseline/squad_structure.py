#!/usr/bin/env python3

import json
import nltk

train_file = "data v1.0/train-v1.0.json"
dev_file = "data v1.0/dev-v1.0.json"

class Question(object):
    def __init__(self, question, qid, answer_start, answer):
        self.question = question
        self.id = qid
        self.answer_start = answer_start
        self.answer = answer
       
        self.question_vector = vectorize(question)
        self.answer_vector = vectorize(answer)
        #self.question_words = nltk.word_tokenizer(question)
        #self.answer_words = nltk.word_tokenizer(answer)


class Paragraph(object):
    def __init__(self, context):
        self.context = context
        self.questions = []

        tokenizer = nltk.data.load("tokenizer/punkt/english.pickle")
        self.sentences = tokenizer.tokenize(context)

        self.words = nltk.word_tokenize(context)


    def add_question(self, question):
        self.questions.append(question)
    


class Article(object):
    def __init__(self, title):
        self.title = title
        self.paragraphs = []
        

    def add_paragraph(self, paragraph):
        self.paragraphs.append(paragraph)




class Data(object):
    def __init__(self, fname):
        self.articles = {}

        with open(fname, "r") as F:
            data = F.read()

        data = json.loads(data)

        for article in data["data"]:
            a = Article(article["title"]) 

            for paragraph in article["paragraphs"]:
                p = Paragraph(paragraph["context"])

                for qa in paragraph["qas"]:
                    for answer in qa["answers"]:
                        p.add_question(Question(qa["question"], qa["id"],
                                                answer["answer_start"], answer["text"]))
        
            
                a.add_paragraph(p)

            self.articles[a.title] = a
    
    
    def add_article(self, article):
        self.articles[article.title] = article



def make_bag(sentence):
    """Generate bag-of-words model vector for the sentence."""

