#!/usr/bin/env python3

import json

with open("predictions.json", "r") as PF:
    predictions = PF.read()

predictions = json.loads(predictions)

with open("data v1.0/dev-v1.0.json", "r") as DF:
    data = DF.read()

data = json.loads(data)

i = 0 
with open("formatted_qas", "w") as QF:
    for article in data["data"]:
        for paragraph in article["paragraphs"]:
            for qa in paragraph["qas"]:
                QF.write(qa["question"] + "\t" + list(predictions.values())[i] + "\n")
                i += 1