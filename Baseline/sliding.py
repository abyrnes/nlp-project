#!/usr/bin/env python3

import nltk
import math

passage = """James the Turtle was always getting into trouble. Sometimes he'd reach into the freezer and empty out all the food. Other times he'd sled on the deck and get a splinter. His aunt Jane tried as hard as she could to keep him out of trouble, but he was sneaky and got into lots of trouble behind her back.
One day, James thought he would go into town and see what kind of trouble he could get into. He went to the grocery store and pulled all the pudding off the shelves and ate two jars. Then he walked to the fast food restaurant and ordered 15 bags of fries. He didn't pay, and instead headed home.
His aunt was waiting for him in his room. She told James that she loved him, but he would have to start acting like a well-behaved turtle.
After about a month, and after getting into lots of trouble, James finally made up his mind to be a better turtle."""

question = "What is the name of the trouble making turtle?"

answers = ["Fries", "Pudding", "James", "Jane"]

P = nltk.word_tokenize(passage)
PW = set(P)
Q = set(nltk.word_tokenize(question))
A = answers

C = {}
IC = {}

for word in P:
    try:
        C[word.lower()] += 1
    except KeyError:
        C[word.lower()] = 1


for word in PW:
    IC[word] = math.log(1 + 1 / C[word.lower()])


SW = [None] * 4
for i in range(0, 4):
    S = set(list(A[i]) + list(Q))

    SW[i] = 0
    for j in range(0, len(P)):
        s = 0
        for w in range(0, len(S)):
            if (j + w < len(P)) and (P[j + w] in S):
                s += IC[P[j + w]]
            else:
                s += 0

        if SW[i] < s:
            SW[i] = s
            

print(SW)
